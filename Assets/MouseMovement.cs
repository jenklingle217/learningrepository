﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class MouseMovement : MonoBehaviour {
    [SerializeField] Rigidbody2D rb;
    [SerializeField] float speed;
    private int count = 0;
    private int lives = 4;
    [SerializeField] Text countText;
    [SerializeField] Text livesText;

    void Start ()
    {
        
    }

    void FixedUpdate ()
    {
        float moveHorizontal = Input.GetAxis("Horizontal") * speed;
        float moveVertical = Input.GetAxis("Vertical") * speed;

        Vector2 movement = new Vector2(moveHorizontal, moveVertical);
        rb.velocity = movement;

        countText.text = "Score: " + count.ToString();
        livesText.text = "Remaining Lives: " + lives.ToString();
    }

    void OnTriggerEnter2D (Collider2D other)
    {
        if (other.gameObject.CompareTag("Cheese"))
        {
            other.gameObject.SetActive(false);
            count++;
        }
  
        if (other.gameObject.CompareTag("Trap"))
        {
            lives--;
        } 
    }
}
